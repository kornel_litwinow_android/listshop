package kornel.litwinow.listshopapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerView_Config {
    private Context mContex;
    private ProductAdapter mProductAdapter;
    public void setConfig(RecyclerView recyclerView, Context context, List<Product>products,List<String>keys){
        mContex = context;
        mProductAdapter = new ProductAdapter(products, keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mProductAdapter);

    }

    class ProductItemView extends RecyclerView.ViewHolder{
        private TextView mTitle;
        private TextView mDescription;

        private String key;

        public ProductItemView(ViewGroup parent){
            super(LayoutInflater.from(mContex).inflate(R.layout.list_item_product,parent,false));

            mTitle = (TextView) itemView.findViewById(R.id.title_product);
            mDescription = (TextView) itemView.findViewById(R.id.description_product);

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContex,ProductDetailsActivity.class);
                intent.putExtra("key",key);
                intent.putExtra("title",mTitle.getText().toString());
                intent.putExtra("description",mDescription.getText().toString());
                mContex.startActivity(intent);
            });
        }
        public void bind(Product product,String key){
            mTitle.setText(product.getTitle());
            mDescription.setText(product.getDescription());
            this.key = key;

        }
    }

    class ProductAdapter extends RecyclerView.Adapter<ProductItemView>{
        private List<Product>mProductList;
        private List<String>mKeys;

        public ProductAdapter(List<Product> mProductList, List<String> mKeys) {
            this.mProductList = mProductList;
            this.mKeys = mKeys;
        }


        @NonNull
        @Override
        public ProductItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ProductItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull ProductItemView holder, int position) {
            holder.bind(mProductList.get(position),mKeys.get(position));
        }

        @Override
        public int getItemCount() {
            return mProductList.size();
        }
    }
}
