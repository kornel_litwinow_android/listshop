package kornel.litwinow.listshopapp;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailsActivity extends AppCompatActivity {
    @BindView(R.id.title_product)
    EditText titleProduct;
    @BindView(R.id.description_product)
    EditText descriptionProduct;
    @BindView(R.id.update_product)
    Button updateProduct;
    @BindView(R.id.delete_product)
    Button deleteProduct;


    private String key;
    private String title;
    private String description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);

        key = getIntent().getStringExtra("key");
        title = getIntent().getStringExtra("title");
        description = getIntent().getStringExtra("description");


        titleProduct.setText(title);
        descriptionProduct.setText(description);


        updateProduct.setOnClickListener(v -> {
            Product product = new Product();
            product.setTitle(titleProduct.getText().toString());
            product.setDescription(descriptionProduct.getText().toString());

            new FirebaseDatabaseHelper().updateProduct(key, product, new FirebaseDatabaseHelper.DataStatus() {
                @Override
                public void DataIsLoaded(List<Product> products, List<String> keys) {

                }

                @Override
                public void DataIsInsterted() {

                }

                @Override
                public void DataIsUpdated() {
                    Toast.makeText(ProductDetailsActivity.this, "Zaaktualizowano produkt", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void DataIsDelete() {

                }
            });
        });

        deleteProduct.setOnClickListener(v -> new FirebaseDatabaseHelper().deleteProduct(key, new FirebaseDatabaseHelper.DataStatus() {
            @Override
            public void DataIsLoaded(List<Product> products, List<String> keys) {

            }

            @Override
            public void DataIsInsterted() {

            }

            @Override
            public void DataIsUpdated() {

            }

            @Override
            public void DataIsDelete() {
                Toast.makeText(ProductDetailsActivity.this, "Produkt został usunięty", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
        }));
        Log.d("", "deleteProduct: delete deletedeletedeletedeletedeletedeletedeletedeletedeletedelete");

    }
}
