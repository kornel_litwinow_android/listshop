package kornel.litwinow.listshopapp;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewProductActivity extends AppCompatActivity {


    @BindView(R.id.title_product)
    EditText titleProduct;
    @BindView(R.id.description_product)
    EditText descriptionProduct;
    @BindView(R.id.add_product)
    Button addProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product);
        ButterKnife.bind(this);


        addProduct.setOnClickListener(v -> {
            Product product = new Product();
            product.setTitle(titleProduct.getText().toString());
            product.setDescription(descriptionProduct.getText().toString());

            new FirebaseDatabaseHelper().addProduct(product, new FirebaseDatabaseHelper.DataStatus() {
                @Override
                public void DataIsLoaded(List<Product> products, List<String> keys) {

                }

                @Override
                public void DataIsInsterted() {
                    Toast.makeText(NewProductActivity.this, "Produkt został dodany"
                            , Toast.LENGTH_LONG).show();
                }

                @Override
                public void DataIsUpdated() {

                }

                @Override
                public void DataIsDelete() {

                }
            });
        });


    }
}
